# About

So I have no clue where I'm going with this. The idea came from this old bash
script I wrote in my first year (second year???) of Uni that was _awesome_.
I could run commands to generate homework templates, note templates, new
courses, etc. Everything was in a git repository and was uploaded to github (or
Dropbox? Both?). Anyway I want to over-engineer a product to make it do
basically the same thing, with an API and potentially a front-end, which is
consumed by a webapp, and if I'm feeling adventurous, a CLI (lol). Anyway this
will probably not be completed because I'm lazy.

Also gitlab is cool and people should support it (and yes, I'm too cheap --
also broke -- so I'm not on the paid plan, but you should be!)

# Brainstorming

## Database

Probably going to use PSQL because I like it.

Structure might be something like:

Student takes many courses which have many aspects (homework, projects, notes, review), which each have things.

So one table of Student with the following fields: `id | name | student_number | year(?) | ... `

A table of courses with the following fields: `id | student_id_fk (or the student number???) | prof | short_description | term (or two fields: start/end date) | ... `

A table of homework with the following fields: `id | course_fk | student_fk | date_due | date_assigned (maybe we can have reminder notification) | ...`

A table of projects with the structure above
Note that projects may have phases ...

A table of notes with the following fields: ` id | course_fk | student_fk | date_taken | ...`

A table of tags with the following field: ` id | tag_name | fk | ...`
